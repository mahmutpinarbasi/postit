//
//  DiContainer.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import Foundation
import UIKit

final class DIContainer {
    
    func makePostListViewController() -> PostListViewController {
        return PostListViewController.instance(from: makePostListViewModel())
    }
    
    func makeNewPostViewController(_ userId: Int, delegate: NewPostViewControllerDelegate?) -> NewPostViewController {
        return NewPostViewController.instance(from: makeNewPostViewModel(userId),
                                              delegate: delegate)
    }
    
    func makeImageDetailViewController(_ image: UIImage) -> ImageDetailViewController {
        return ImageDetailViewController.instance(from: makeImageDetailViewModel(image))
    }
    
    private func makeImageDetailViewModel(_ image: UIImage) -> ImageDetailViewModelProtocol {
        return ImageDetailViewModel.init(image)
    }
    
    private func makeNewPostViewModel(_ userId: Int) -> NewPostViewModelProtocol {
        return NewPostViewModel(userId, postService: makePostService())
    }
    
    private func makePostListViewModel() -> PostListViewModelProtocol {
        return PostListViewModel()
    }
    
    private func makeUserService() -> UserServiceProtocol {
        return UserService()
    }
    
    private func makePostService() -> PostServiceProtocol {
        return PostService()
    }
}
