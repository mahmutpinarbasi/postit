//
//  ProfileImageView.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import UIKit

/// ProfileImageView
/// An ImageView with rounded corners
class ProfileImageView: UIImageView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        } set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}
