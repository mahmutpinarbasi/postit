//
//  PostTextView.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import UIKit

class PostTextView: UITextView {
    
    private static let defaultFont = UIFont.systemFont(ofSize: 17.0)

    var placeholderLabel: UILabel = {
        let label = UILabel()
        label.text = "What're you thinking?"
        label.textColor = UIColor.lightGray
        label.sizeToFit()
        return label
    }()

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonSetup()
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        commonSetup()
    }
    
    private func commonSetup() {
        autocorrectionType = .no
        font = Self.defaultFont
        addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: Self.defaultFont.pointSize / 2)
        NotificationCenter.default.addObserver(self, selector: #selector(textViewDidChange), name: UITextView.textDidChangeNotification, object: nil)
    }
    
    @objc
    private func textViewDidChange() {
        placeholderLabel.isHidden = !self.text.isEmpty
    }
}
