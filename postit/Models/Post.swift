//
//  Post.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import Foundation
import UIKit

struct Post {
    
    let text: String
    let images: [UIImage]?
    let userId: Int
}
