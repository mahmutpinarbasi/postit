//
//  User.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import Foundation
import UIKit

struct User: Decodable {
    
    let id: Int
    let username: String
    let displayName: String
    let profilePhotoName: String
    
    var profilePhoto: UIImage {
        return UIImage(named: profilePhotoName) ?? Self.defaultImage
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case username
        case displayName
        case profilePhotoName = "profilePhoto"
    }
    
    
    static var defaultImage: UIImage {
        get {
            return UIImage.init(systemName: "person")!
        }
    }
}

