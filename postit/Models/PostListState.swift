//
//  RequestState.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 8.04.2022.
//

import Foundation

public enum PostListState<T> {
    
    case idle
    case newPost(T, IndexPath)
    case applyFilter
    case failure(Error)
}
