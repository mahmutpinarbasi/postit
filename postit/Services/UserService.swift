//
//  UserService.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import Foundation

enum UserServiceError: Error {
    case jsonNotFound
    case dataCorruption
}

typealias UserCompletion = ((Result<[User], Error>) -> ())

protocol UserServiceProtocol {
    
    func loadUsers(_ completion: @escaping UserCompletion)
}

class UserService: UserServiceProtocol {
    
    private let jsonParseQueue = DispatchQueue.global(qos: .default)
    
    func loadUsers(_ completion: @escaping UserCompletion) {
        getUsersFromFile(completion)
    }
    
    
    private func getUsersFromFile(_ completion: @escaping UserCompletion) {
        jsonParseQueue.async {
            guard let path = Bundle.main.path(forResource: "users", ofType: "json") else {
                completion(Result.failure(UserServiceError.jsonNotFound))
                return
            }
            
            do {
                let url = URL.init(fileURLWithPath: path)
                let data = try Data(contentsOf: url)
                let userList = try JSONDecoder().decode([User].self, from: data)
                completion(Result.success(userList))
            } catch {
                completion(Result.failure(UserServiceError.dataCorruption))
            }
        }
    }
}
