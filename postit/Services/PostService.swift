//
//  PostService.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import Foundation
import UIKit

protocol PostServiceProtocol {
    
    func sendPost(fromUser userId: Int, text: String, images: [UIImage]?) -> Post
}

class PostService: PostServiceProtocol {
    
    func sendPost(fromUser userId: Int, text: String, images: [UIImage]?) -> Post {
        let post = Post(text: text, images: images, userId: userId)
        return post
    }
}
