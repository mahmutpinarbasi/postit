//
//  ImageDetailViewController.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 8.04.2022.
//

import UIKit

class ImageDetailViewController: BaseViewController {

    @IBOutlet private weak var imageView: UIImageView!
    var viewModel: ImageDetailViewModelProtocol!
    
    static func instance(from viewModel: ImageDetailViewModelProtocol) -> ImageDetailViewController {
        let vc = ImageDetailViewController(nibName: "ImageDetailViewController", bundle: Bundle.main)
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareViewComponents()
    }
    
    private func prepareViewComponents() {
        imageView.image = viewModel.image
    }
}
