//
//  PostListViewController.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import UIKit

class PostListViewController: UIViewController {
    
    private var viewModel: PostListViewModelProtocol!
    @IBOutlet weak var tableView: UITableView!
    
    static func instance(from viewModel: PostListViewModelProtocol) -> PostListViewController {
        let vc = PostListViewController(nibName: "PostListViewController", bundle: Bundle.main)
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareViewComponents()
        bindViewModel()
    }
    
    // MARK: - Private -
    private func bindViewModel() {
        self.viewModel.stateChanged = { [weak self] (newState) in
            guard let strongSelf = self else { return }
            
            switch newState {
            case .newPost(_, let indexPath):
                strongSelf.tableView.insertRows(at: [indexPath], with: .top)
            case .applyFilter:
                strongSelf.tableView.reloadData()
            case .failure(let error):
                strongSelf.showError(with: error)
            case .idle:
                strongSelf.tableView.reloadData()
                break
            }
        }
        
        UserController.shared.didChangeCurrentUser = { [weak self] newUser in
            guard let strongSelf = self else { return }
            
            strongSelf.setupUserButton()
        }
    }
    
    func prepareViewComponents() {
        tableView.register(UINib.init(nibName: "PostCell", bundle: nil), forCellReuseIdentifier: "PostCell")
        
        let postButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(postButtonTapped))
        
        let filterButton = UIBarButtonItem.init(title: "Filter", style: .plain, target: self, action: #selector(filterTapped))
        navigationItem.rightBarButtonItems = [postButton, filterButton]
    }
    
    @objc
    private func postButtonTapped() {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate,
              let currentUser = UserController.shared.currentUser else {
            return
        }
        let viewController = delegate.diContainer.makeNewPostViewController(currentUser.id, delegate: self)
        let navigationController = BaseNavigationController(rootViewController: viewController)
        present(navigationController, animated: true, completion: nil)
    }
    
    @objc
    private func filterTapped() {
        viewModel.switchFilter()
    }
    
    @objc
    private func switchUser() {
        let alert = UIAlertController.init(title: "Switch User", message: "Select current user", preferredStyle: .actionSheet)
        for (id, user) in UserController.shared.users {
            if id == UserController.shared.currentUser.id {
                continue
            }
            
            let action = UIAlertAction.init(title: user.displayName, style: .default) { [weak self] action in
                guard let strongSelf = self else { return }
                strongSelf.viewModel.switchUser(user.id)
            }
            alert.addAction(action)
        }
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func showError(with error: Error) {
        let alertController = UIAlertController.init(
            title: "Error",
            message: error.localizedDescription,
            preferredStyle: .alert)
        
        let action = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func setupUserButton() {
        let targetSize = CGSize.init(width: 44.0, height: 44.0)
        let image = UserController.shared.currentUser.profilePhoto.resize(to: targetSize )
        let button = UIButton()
        button.bounds = CGRect.init(origin: CGPoint.zero, size: targetSize)
        button.layer.cornerRadius = targetSize.height / 2
        button.clipsToBounds = true
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(switchUser), for: .touchUpInside)
        let switchButton = UIBarButtonItem.init(customView: button)
        navigationItem.leftBarButtonItem = switchButton
    }
}

// MARK: - UITableViewDataSource -
extension PostListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostCell
        cell.delegate = self
        cell.setup(from: viewModel.item(at: indexPath))
        return cell
    }
}

// MARK: - NewPostViewControllerDelegate -
extension PostListViewController: NewPostViewControllerDelegate {
    func newPostViewController(_ viewController: NewPostViewController, didSend post: Post) {
        viewController.dismiss(animated: true) {
            self.viewModel.add(post)
        }
    }
}

extension PostListViewController: PostCellDelegate {
    
    func postCell(_ cell: PostCell, didSelect image: UIImage) {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let vc = delegate.diContainer.makeImageDetailViewController(image)
        present(vc, animated: true, completion: nil)
    }
}
