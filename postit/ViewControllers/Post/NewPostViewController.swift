//
//  NewPostViewController.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import UIKit

protocol NewPostViewControllerDelegate: AnyObject {
    
    func newPostViewController(_ viewController: NewPostViewController, didSend post: Post)
}

class NewPostViewController: BaseViewController, UINavigationControllerDelegate {

    private var viewModel: NewPostViewModelProtocol!
    weak var delegate: NewPostViewControllerDelegate?
    @IBOutlet private weak var postTextView: PostTextView!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    private static let emptyImageCellIdentifier = "EmptyImageCell"
    private static let imageCellIdentifier = "ImageCell"
    
    static func instance(from viewModel: NewPostViewModelProtocol, delegate: NewPostViewControllerDelegate?) -> NewPostViewController {
        let vc = NewPostViewController(nibName: "NewPostViewController", bundle: Bundle.main)
        vc.viewModel = viewModel
        vc.delegate = delegate
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareViewComponents()
        bindViewModel()
    }
    
    private func bindViewModel() {
        viewModel.postableState = { [weak self] canPost in
            guard let strongSelf = self else { return }
            
            let sendItem = strongSelf.navigationItem.rightBarButtonItem
            sendItem?.isEnabled = canPost
        }
    }
    
    private func prepareViewComponents() {
        let sendItem = UIBarButtonItem.init(title: "Send", style: .plain, target: self, action: #selector(sendTapped))
        navigationItem.rightBarButtonItem = sendItem
        sendItem.isEnabled = false
        
        collectionView.register(UINib.init(nibName: "EmptyImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: Self.emptyImageCellIdentifier)
        collectionView.register(UINib.init(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: Self.imageCellIdentifier)
    }
    
    @objc
    private func sendTapped() {
        let post = viewModel.sendPost()
        delegate?.newPostViewController(self, didSend: post)
    }
}

extension NewPostViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Self.emptyImageCellIdentifier, for: indexPath) as! EmptyImageCollectionViewCell
            cell.delegate = self
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Self.imageCellIdentifier, for: indexPath) as! ImageCollectionViewCell
            cell.setImage(viewModel.image(at: indexPath))
            return cell
        }
    }
}

// MARK: - UITextViewDelegate -
extension NewPostViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        viewModel.postText = textView.text
    }
}

extension NewPostViewController: EmptyImageCollectionViewCellDelegate {
    
    func emptyImageCollectionViewCell(_ cell: EmptyImageCollectionViewCell, didTapped addButton: UIButton) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.sourceType = .photoLibrary
        present(pickerController, animated: true, completion: nil)
    }
}

extension NewPostViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            picker.dismiss(animated: true, completion: nil)
            return
        }
        
        viewModel.addImage(image)
        picker.dismiss(animated: true, completion: nil)
        collectionView.reloadData()
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
    }
    
}
