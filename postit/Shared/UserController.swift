//
//  UserController.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 8.04.2022.
//

import Foundation

class UserController {
    
    static let shared = UserController()
    let userService: UserServiceProtocol
    
    private(set) var users: [Int:User] = [:]
    var currentUser: User!
    var didChangeCurrentUser: ((_ newUser: User) -> Void)?
//    var stateChanged: ((_ newState: RequestState<[PostCellViewModel]>) -> Void)? { get set }
    
    
    init() {
        self.userService = UserService()
        loadUsers()
    }
    
    func updateCurrentUser(_ newUserId: Int) -> Bool {
        guard let user = users[newUserId] else {
            return false
        }
        
        currentUser = user
        didChangeCurrentUser?(currentUser)
        return true
    }
    
    // MARK: - Private
    private func loadUsers() {
        userService.loadUsers { [weak self] result in
            guard let strongSelf = self else { return }
            
            DispatchQueue.main.async {
                switch result {
                case .success(let newUsers):
                    let newUsersDic = Dictionary(uniqueKeysWithValues: newUsers.map({ ($0.id, $0) }))
                    newUsersDic.forEach { (id, user) in
                        strongSelf.users[id] = user
                    }
                    let newCurrentUser = newUsers[0]
                    _ = strongSelf.updateCurrentUser(newCurrentUser.id)
                case .failure(let error):
                    print("error while loading users...\(error)")
                }
            }
        }
    }
}
