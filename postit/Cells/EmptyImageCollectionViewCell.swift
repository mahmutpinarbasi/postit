//
//  EmptyImageCollectionViewCell.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 8.04.2022.
//

import UIKit

protocol EmptyImageCollectionViewCellDelegate: AnyObject {
    
    func emptyImageCollectionViewCell(_ cell: EmptyImageCollectionViewCell, didTapped addButton: UIButton)
}

class EmptyImageCollectionViewCell: UICollectionViewCell {

    
    weak var delegate: EmptyImageCollectionViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func addButtonTapped(_ sender: UIButton) {
        delegate?.emptyImageCollectionViewCell(self, didTapped: sender)
    }
}
