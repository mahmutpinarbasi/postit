//
//  PostCell.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import UIKit

protocol PostCellDelegate: AnyObject {
    
    func postCell(_ cell: PostCell, didSelect image: UIImage)
}

class PostCell: UITableViewCell {

    @IBOutlet private weak var userImageView: ProfileImageView!
    @IBOutlet private weak var labelUsername: UILabel!
    @IBOutlet private weak var labelDisplayName: UILabel!
    @IBOutlet private weak var labelText: UILabel!
    @IBOutlet private weak var postImageView: UIImageView!

    weak var delegate: PostCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(from model: PostCellViewModel?) {
        guard let model = model else {
            return
        }
        userImageView.image = model.userImage
        labelText.text = model.postText
        labelUsername.text = "@\(model.username)"
        labelDisplayName.text = model.userDisplayName
        postImageView.image = model.postImage
        
        if model.postImage != nil {
            let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(imageTapped))
            postImageView.addGestureRecognizer(tapRecognizer)
        }
    }
    
    @objc
    private func imageTapped() {
        guard let image = postImageView.image else {
            return
        }
        delegate?.postCell(self, didSelect: image)
    }
    
}
