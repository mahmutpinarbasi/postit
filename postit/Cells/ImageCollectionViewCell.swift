//
//  ImageCollectionViewCell.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 8.04.2022.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak private var postImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setImage(_ image: UIImage?) {
        postImage.image = image
    }

}
