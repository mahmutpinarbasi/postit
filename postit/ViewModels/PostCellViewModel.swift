//
//  PostCellViewModel.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import Foundation
import UIKit

struct PostCellViewModel {
    
    let postText: String
    let username: String
    let userDisplayName: String
    let userImage: UIImage
    let postImage: UIImage?
    
    static func instance(from post: Post, user: User) -> PostCellViewModel {
        return PostCellViewModel(
            postText: post.text,
            username: user.username,
            userDisplayName: user.displayName,
            userImage: user.profilePhoto,
            postImage: post.images?.first)
    }
}
