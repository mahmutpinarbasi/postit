//
//  PostListViewModel.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import Foundation

enum FilterOption {
    case all, mine
}

protocol PostListViewModelProtocol {
    var numberOfItems: Int { get }
    var stateChanged: ((_ newState: PostListState<[PostCellViewModel]>) -> Void)? { get set }
    var users: [Int:User] { get }
    
    func item(at indexPath: IndexPath) -> PostCellViewModel?
    
    /// adds new post  to the first index of existing `posts` array
    func add(_ post: Post)
    
    /// Switches to new user among users
    func switchUser(_ newUserId: Int)
    
    /// Updates the existng filter according `currentFilter`.
    /// Apply from mine to all or vice versa
    func switchFilter()
}

class PostListViewModel: PostListViewModelProtocol {
        
    private var items: [PostCellViewModel] = []
    private var filteredItems: [PostCellViewModel] = []
    private var currentFilter = FilterOption.all {
        didSet {
            self.state = PostListState.applyFilter
        }
    }
    
    var users: [Int:User] = UserController.shared.users
    
    init() {
        self.state = .idle
    }
    
    // MARK: - MovieListViewModelProtocol
    private(set) var state: PostListState<[PostCellViewModel]> = .idle {
        didSet {
            self.stateChanged?(self.state)
        }
    }
    
    var stateChanged: ((_ newState: PostListState<[PostCellViewModel]>) -> Void)?
    
    func add(_ post: Post) {
        let cellModel = PostCellViewModel.instance(from: post, user: UserController.shared.currentUser)
        self.items.insert(cellModel, at: 0)
        self.filteredItems.insert(cellModel, at: 0)
        let indexPath = IndexPath.init(item: 0, section: 0)
        self.state = PostListState.newPost([cellModel], indexPath)
    }
    
    var numberOfItems: Int {
        return filteredItems.count
    }
    
    func item(at indexPath: IndexPath) -> PostCellViewModel? {
        if (indexPath.row >= filteredItems.count) { return nil }
        
        return filteredItems[indexPath.row]
    }
    
    func switchUser(_ newUserId: Int) {
        _ = UserController.shared.updateCurrentUser(newUserId)
        applyFilter(with: currentFilter)
    }
    
    func switchFilter() {
        switch (currentFilter) {
        case .all:
            applyFilter(with: .mine)
        case .mine:
            applyFilter(with: .all)
        }
    }
    
    private func applyFilter(with option: FilterOption) {
        filteredItems.removeAll()
        switch (option) {
        case .all:
            filteredItems.append(contentsOf: items)
        case .mine:
            let myPosts = items.filter({ $0.username == UserController.shared.currentUser.username })
            filteredItems.append(contentsOf: myPosts)
        }
        currentFilter = option
    }
}
