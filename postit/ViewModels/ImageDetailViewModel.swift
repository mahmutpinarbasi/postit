//
//  ImageDetailViewModel.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 8.04.2022.
//

import Foundation
import UIKit

protocol ImageDetailViewModelProtocol {
    
    var image: UIImage { get }
}

class ImageDetailViewModel: ImageDetailViewModelProtocol {
    
    let image: UIImage
    
    init(_ image: UIImage) {
        self.image = image
    }
}
