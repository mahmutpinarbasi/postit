//
//  NewPostViewModel.swift
//  postit
//
//  Created by Mahmut Pınarbaşı on 7.04.2022.
//

import Foundation
import UIKit

protocol NewPostViewModelProtocol {
    
    func sendPost() -> Post
    
    var postableState: ((_ canPost: Bool) -> Void)? { get set }
    var postText: String { get set }
    
    func addImage(_ image: UIImage)
    
    /// number of images only
    var numberOfImages: Int { get }
    
    /// number of items including "add image" cell
    var numberOfItems: Int { get }
    
    func image(at indexPath: IndexPath) -> UIImage?
}

class NewPostViewModel: NewPostViewModelProtocol {
    
    let userId: Int
    let postService: PostServiceProtocol
    var canSendPost: Bool = false
    var postableState: ((_ canPost: Bool) -> Void)?
    
    var images: [UIImage] = []
    
    var _postText: String = ""
    var postText: String {
        set {
            if newValue.trimmed().isEmpty {
                self.postableState?(false)
            } else {
                self.postableState?(true)
            }
            _postText = newValue
        } get {
            return _postText
        }
    }
    
    init(_ userId: Int, postService: PostServiceProtocol) {
        self.userId = userId
        self.postService = postService
        self.postText = ""
    }
    
    func sendPost() -> Post {
        return postService.sendPost(fromUser: userId, text: postText, images: images)
    }
    
    func addImage(_ image: UIImage) {
        images.append(image)
    }
    
    var numberOfImages: Int {
        return images.count
    }
    
    var numberOfItems: Int {
        return numberOfImages + 1
    }
    
    func image(at indexPath: IndexPath) -> UIImage? {
        let actualIndex = indexPath.row - 1
        return images[actualIndex]
    }
}

fileprivate extension String {
    func trimmed() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}
