postit


A basic app to demonstrate creating & viewing posts created by 3 users.
Users are read from **Assets/users.json** file.

Currently you'll be able to
 1. Create Post with single image ( adding multiple image is supported but only first one will be viewed)
 2. View Posts
 3. Switch Between 3 users; Not tested but number of users can be increased by adding more users in to users.json file.
 4. Apply filter to show All posts or **current** users posts only.
   
